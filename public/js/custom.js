$(document).ready(function(){

    var days = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata'];

    $.each(days, function(index, value){
        if ($("." + value + "-free").is(":checked"))
        {

            $("." + value + "_schedule_start_hour").attr('disabled', 'true');
            $("." + value + "_schedule_start_min").attr('disabled', 'true');

            $("." + value + "_schedule_end_hour").attr('disabled', 'true');
            $("." + value + "_schedule_end_min").attr('disabled', 'true');
        }

        $("." + value + "-free").change(function(){

        if ($(this).is(":checked"))
        {

            $("." + value + "_schedule_start_hour").prop('disabled', 'true');
            $("." + value + "_schedule_start_min").prop('disabled', 'true');

            $("." + value + "_schedule_end_hour").prop('disabled', 'true');
            $("." + value + "_schedule_end_min").prop('disabled', 'true');
        }

        if ($(this).is(":not(:checked)"))
        {

            $("." + value + "_schedule_start_hour").removeAttr('disabled');
            $("." + value + "_schedule_start_min").removeAttr('disabled');

            $("." + value + "_schedule_end_hour").removeAttr('disabled');
            $("." + value + "_schedule_end_min").removeAttr('disabled');
        }




        })
        })
    });





