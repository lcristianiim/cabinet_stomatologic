<?php

class AuthenticationController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function authenticate()
    {
        if (Auth::check()) {
            return View::make('logged');
        }

        $input = Input::all();

            if (Auth::attempt(array('email' => $input['email'], 'password' => $input['password'])))
            {
                return View::make('logged');
            } else {
                return View::make('sign_in')->with(array(
                    'input' => $input
                ));
            }

        }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('homepage');
    }

    public function state()
    {
        if (Auth::check())
        {
            return 'User is logged';
        } else {
            return 'No user is logged';
        }
    }
    public function create_user(){
        $user = new User;
        $user->email = 'lcristianiim@yahoo.com';
        $user->password =  Hash::make('aaa');

        $user->save();

        return Redirect::to('autentificare');
    }

}
