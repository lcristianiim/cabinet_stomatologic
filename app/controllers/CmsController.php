<?php
/**
 * Created by PhpStorm.
 * User: Cristian
 * Date: 9/20/14
 * Time: 12:58 PM
 */

class CmsController extends BaseController {

    public function doctors(){
       if ( Auth::check() )
       {

           $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata'];
           $doctors = Doctor::all();

           return View::make('doctors_cms')->with(array(
               'days_of_the_week' => $days_of_the_week,
               'doctors' => $doctors
           ));
       } else {
           return View::make('sign_in');
       }
    }

    public function new_doctor_store(){
        $input = Input::all();


        $validation = Validator::make(Input::all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email'
        ]);

        if ($validation->fails()){
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }

        $doctor = new Doctor;
        $doctor->first_name = $input['first_name'];
        $doctor->last_name = $input['last_name'];
        $doctor->phone = $input['phone'];
        $doctor->email = $input['email'];

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

        foreach ($days_of_the_week as $day) {
            if (
                  isset($input[$day.'_schedule_start_hour'])
                & isset($input[$day.'_schedule_start_min'])
                & isset($input[$day.'_schedule_end_hour'])
                & isset($input[$day.'_schedule_end_min'])
            )
            {

                $schedule_start = $input[$day.'_schedule_start_hour'] . ":" . $input[$day.'_schedule_start_min'];
                $schedule_end = $input[$day.'_schedule_end_hour'] . ":" . $input[$day.'_schedule_end_min'];

                $property1 = $day."_schedule_start";
                $property2 = $day."_schedule_end";

                $doctor->$property1 = $schedule_start;
                $doctor->$property2 = $schedule_end;
            }
        }

        $number_of_doctors = Doctor::count();
        $doctor->order = $number_of_doctors + 1;
        $doctor->visibility = 1;

        $doctor->save();

        return Redirect::to('doctori');
    }

    public function delete_doctor($id){
        $doctor = Doctor::find($id);
        $doctor->delete();

        return Redirect::to('doctori');
    }

    public function edit_doctor($id){
        $input = Input::all();

        $doctor = Doctor::find($id);

        $doctor->first_name = $input['first_name'];
        $doctor->last_name = $input['last_name'];
        $doctor->phone = $input['phone'];
        $doctor->email = $input['email'];

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

        foreach ($days_of_the_week as $day) {
            if (
                isset($input[$day.'_schedule_start_hour'.$id])
                & isset($input[$day.'_schedule_start_min'.$id])
                & isset($input[$day.'_schedule_end_hour'.$id])
                & isset($input[$day.'_schedule_end_min'.$id])
            )
            {
                $schedule_start = $input[$day.'_schedule_start_hour'.$id] . ":" . $input[$day.'_schedule_start_min'.$id];
                $schedule_end = $input[$day.'_schedule_end_hour'.$id] . ":" . $input[$day.'_schedule_end_min'.$id];

                $property1 = $day."_schedule_start";
                $property2 = $day."_schedule_end";

                $doctor->$property1 = $schedule_start;
                $doctor->$property2 = $schedule_end;
            } else {
                $property1 = $day."_schedule_start";
                $property2 = $day."_schedule_end";

                $doctor->$property1 = null;
                $doctor->$property2 = null;
            }

        }

        $doctor->save();
        return Redirect::to('doctori');
    }

    public function phone_schedule(){

        $phone_schedule = DB::table('phone_schedule')->first();

        if (is_null($phone_schedule)) {
            $phone_schedule_set = 0;

            return View::make('phone_schedule')->with(array(
                'phone_schedule_set' => $phone_schedule_set
            ));

        } else {
            $phone_schedule_set = 1;

            return View::make('phone_schedule')->with(array(
                'phone_schedule_set' => $phone_schedule_set,
                'start_time' => $phone_schedule->start_time,
                'end_time' => $phone_schedule->end_time,
                'phone_1' => $phone_schedule->phone_1,
                'phone_2' => $phone_schedule->phone_2
            ));
        }
    }
    public function edit_phone_schedule(){
        $input = Input::all();

        $phone_schedule = PhoneSchedule::get();

        $phone_schedule[0]->start_time = $input['start_time'];
        $phone_schedule[0]->end_time = $input['end_time'];
        $phone_schedule[0]->phone_1 = $input['phone_1'];
        $phone_schedule[0]->phone_2 = $input['phone_2'];

        $phone_schedule[0]->save();

        return Redirect::to('programari_telefonice');
    }

    public function descriptions(){

        $descriptions = DB::table('descriptions')->first();

        if (is_null($descriptions))
        {

            return View::make('descriptions')->with(array(
                'descriptions_empty' => 1
            ));

        } else {

            $descriptions = Description::all();

            return View::make('descriptions')->with(array(
                'descriptions_empty' => 0,
                'descriptions' => $descriptions
            ));

        }
    }

    public function add_new_description(){
        $input =  Input::all();

        $description = new Description;
        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();

        return Redirect::to('descriere_cabinet');
    }

    public function delete_description($id){
        $description = Description::find($id);

        $description->delete();

        return Redirect::to('descriere_cabinet');
    }

    public function edit_description($id){
        $input = Input::all();
        $description = Description::find($id);

        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();
        return Redirect::to('descriere_cabinet');
    }

    public function professional_services(){

        $descriptions = ServicesDescription::all();
        $services =Service::all();

        return View::make('professional_services')->with(array(
            'descriptions' => $descriptions,
            'services' => $services
        ));
    }

    public function add_new_service_description(){
        $input =  Input::all();

        $description = new ServicesDescription;
        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();

        return Redirect::to('servicii_profesionale_cms');
    }

    public function delete_services_description($id){
        $description = ServicesDescription::find($id);

        $description->delete();

        return Redirect::to('servicii_profesionale_cms');
    }

    public function edit_service_description($id){
        $input = Input::all();
        $description = ServicesDescription::find($id);

        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();
        return Redirect::to('servicii_profesionale_cms');
    }

    public function add_new_service(){
        $input =  Input::all();

        $description = new Service;
        $description->name = $input['name'];
        $description->price = $input['price'];

        $description->save();

        return Redirect::to('servicii_profesionale_cms');
    }

    public function delete_service($id){
        $description = Service::find($id);

        $description->delete();

        return Redirect::to('servicii_profesionale_cms');
    }

    public function edit_service($id){
        $input = Input::all();
        $description = Service::find($id);

        $description->name = $input['name'];
        $description->price = $input['price'];

        $description->save();
        return Redirect::to('servicii_profesionale_cms');
    }

    public function partners(){
        $descriptions = PartnerDescription::all();
        $services = Partner::all();

        return View::make('partners')->with(array(
            'descriptions' => $descriptions,
            'services' => $services
        ));
    }

    public function add_new_partner_description(){
        $input =  Input::all();

        $description = new PartnerDescription;
        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();

        return Redirect::to('partners');
    }

    public function delete_partners_description($id){
        $description = PartnerDescription::find($id);

        $description->delete();

        return Redirect::to('partners');
    }

    public function edit_partners_description($id){
        $input = Input::all();
        $description = PartnerDescription::find($id);

        $description->title = $input['title'];
        $description->text = $input['content'];

        $description->save();

        return Redirect::to('partners');
    }

    public function add_new_partner(){
        $input =  Input::all();

        $description = new Partner;
        $description->name = $input['name'];
        $description->link = $input['link'];

        $description->save();

        return Redirect::to('partners');
    }

    public function delete_partner($id){
        $description = Partner::find($id);

        $description->delete();

        return Redirect::to('partners');
    }

    public function edit_partner($id){
        $input = Input::all();
        $description = Partner::find($id);

        $description->name = $input['name'];
        $description->link = $input['link'];

        $description->save();

        return Redirect::to('partners');
    }

    public function contact(){
        $contact = Contact::find(1);

        return View::make('contact_cms')->with(array(
            'contact' => $contact
        ));
    }

    public function edit_contact_details(){
        $input = Input::all();

        $contact = Contact::find(1);

        $contact->name = $input['name'];
        $contact->street = $input['street'];
        $contact->neighborhood = $input['neighborhood'];
        $contact->city = $input['city'];
        $contact->postal_code = $input['postal_code'];
        $contact->phone = $input['phone'];
        $contact->fax = $input['fax'];

        $contact->save();

        return Redirect::to('contact_cms');

    }

    public function why_us(){
        $descriptions = WhyUs::all();

        return View::make('why_us')->with(array(
            'descriptions' => $descriptions
        ));
    }

    public function delete_why_us($id){
        $description = WhyUs::find($id);

        $description->delete();

        return Redirect::to('de_ce_cabinetul_nostru');
    }

    public function add_new_why_us(){
        $input =  Input::all();

        $description = new WhyUs();
        $description->content = $input['content'];

        $description->save();

        return Redirect::to('de_ce_cabinetul_nostru');
    }

    public function edit_why_us($id){
        $input = Input::all();
        $description = WhyUs::find($id);

        $description->content = $input['content'];

        $description->save();

        return Redirect::to('de_ce_cabinetul_nostru');
    }
} 