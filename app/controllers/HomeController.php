<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
        $doctors = Doctor::all();
        $descriptions = Description::all();
        $contact = Contact::find(1);
        $why_us = WhyUs::all();
        $phone_schedule = PhoneSchedule::find(1);

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

		return View::make('homepage')->with([
            'doctors' => $doctors,
            'descriptions' => $descriptions,
            'contact' => $contact,
            'why_us' => $why_us,
            'phone_schedule' => $phone_schedule,
            'days_of_the_week' => $days_of_the_week
        ]);
	}

    public function servicii_profesionale(){

        $doctors = Doctor::all();
        $descriptions = ServicesDescription::all();
        $services = Service::all();
        $contact = Contact::find(1);
        $why_us = WhyUs::all();
        $phone_schedule = PhoneSchedule::find(1);

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

        return View::make('servicii_profesionale')->with([
            'doctors' => $doctors,
            'descriptions' => $descriptions,
            'services' => $services,
            'contact' => $contact,
            'why_us' => $why_us,
            'phone_schedule' => $phone_schedule,
            'days_of_the_week' => $days_of_the_week
        ]);
    }

    public function parteneri(){

        $doctors = Doctor::all();
        $descriptions = PartnerDescription::all();
        $partners = Partner::all();
        $contact = Contact::find(1);
        $why_us = WhyUs::all();
        $phone_schedule = PhoneSchedule::find(1);

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

        return View::make('parteneri')->with([
            'doctors' => $doctors,
            'descriptions' => $descriptions,
            'partners' => $partners,
            'contact' => $contact,
            'why_us' => $why_us,
            'phone_schedule' => $phone_schedule,
            'days_of_the_week' => $days_of_the_week
        ]);
    }

    public function contact()
    {
        $doctors = Doctor::all();
//        $principal_doctor = DB::table('doctors')
//            ->where('first_name', '=', 'Szabo')
//            ->orWhere('last_name', 'Cristian')
//            ->get();

        $contact = Contact::find(1);
        $why_us = WhyUs::all();
        $phone_schedule = PhoneSchedule::find(1);

        $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

        return View::make('contact')->with([
            'doctors' => $doctors,
            'contact' => $contact,
//            'principal_doctor' => $principal_doctor,
            'why_us' => $why_us,
            'phone_schedule' => $phone_schedule,
            'days_of_the_week' => $days_of_the_week
        ]);
    }

}
