<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('doctors', function(Blueprint $table){

            $table->increments('id');

            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('phone', 20);
            $table->string('email');

            $days_of_the_week = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'];

            foreach ($days_of_the_week as $day){
                $table->string($day . '_schedule_start');
                $table->string($day . '_schedule_end');
            }
            $table->string('order');
            $table->string('visibility');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('doctors');
	}

}
