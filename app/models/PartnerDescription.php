<?php

/**
 * @property string thumbnail
 */
class PartnerDescription extends Eloquent  {

    protected $table = 'partners-descriptions';
}