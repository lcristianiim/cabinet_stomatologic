<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::get('/', 'HomeController@showWelcome');
Route::get('homepage', 'HomeController@showWelcome');


Route::get('servicii_profesionale','HomeController@servicii_profesionale' );
Route::get('parteneri','HomeController@parteneri');
Route::get('contact','HomeController@contact');


Route::get('logged', function()
{
    return View::make('logged');
});

Route::get('administrare', function()
{
    if (Auth::check())
    {
        $action = 'authenticate';
        return App::make('AuthenticationController')->$action();
    }
    else {
        return View::make('sign_in');
    }
});

Route::post('authenticate', 'AuthenticationController@authenticate');
Route::get('logout', 'AuthenticationController@logout');

Route::post('new_doctor_store', 'CmsController@new_doctor_store');


Route::get('doctori', 'CmsController@doctors');

Route::get('delete_doctor/{id}', 'CmsController@delete_doctor');
Route::post('edit_doctor/{id}', 'CmsController@edit_doctor');

Route::get('programari_telefonice', 'CmsController@phone_schedule');
Route::post('edit_phone_schedule','CmsController@edit_phone_schedule');

Route::get('descriere_cabinet', 'CmsController@descriptions');
Route::post('adauga_descriere_noua','CmsController@add_new_description');
Route::get('sterge_descriere/{id}','CmsController@delete_description');
Route::post('editare_descriere/{id}','CmsController@edit_description');



Route::get('servicii_profesionale_cms','CmsController@professional_services');
Route::post('adauga_descriere_servicii_noua','CmsController@add_new_service_description');
Route::get('sterge_descrierea_serviciului/{id}','CmsController@delete_services_description');
Route::post('editare_descriere_serviciu/{id}','CmsController@edit_service_description');
Route::post('adauga_serviciu_nou','CmsController@add_new_service');
Route::get('sterge_serviciul/{id}','CmsController@delete_service');
Route::post('editare_serviciu/{id}','CmsController@edit_service');

Route::get('partners','CmsController@partners');
Route::post('adauga_descriere_parteneriat','CmsController@add_new_partner_description');
Route::get('sterge_descrierea_parteneriatului/{id}','CmsController@delete_partners_description');
Route::post('editare_descriere_parteneriat/{id}','CmsController@edit_partners_description');
Route::post('adauga_partener_nou','CmsController@add_new_partner');
Route::get('sterge_partenerul/{id}','CmsController@delete_partner');
Route::post('editare_partener/{id}','CmsController@edit_partner');

Route::get('contact_cms','CmsController@contact');
Route::post('edit_contact_details', 'CmsController@edit_contact_details');

Route::get('de_ce_cabinetul_nostru','CmsController@why_us');
Route::post('adauga_de_ce_nou','CmsController@add_new_why_us');
Route::get('sterge_de_ce/{id}','CmsController@delete_why_us');
Route::post('editare_de_ce_cabinetul_nostru/{id}','CmsController@edit_why_us');

    // To be deleted
Route::get('create_user', 'AuthenticationController@create_user');
Route::get('state', 'AuthenticationController@state');

Route::get('full_sb', function()
{
    return View::make('full_sb');
});

