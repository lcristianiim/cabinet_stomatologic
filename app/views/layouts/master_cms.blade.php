<!DOCTYPE HTML>
<html>

<head>
    <!--
****************************************************
(c) WebSite Name

(c)2014 Cristian
        lcristianiim@yahoo.com

Design & Development by Cristian

****************************************************
-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">

    <meta name="author" content="">
    <meta name="publisher" content="">
    <meta name="page-topic" content="">
    <meta name="page-type" content="">
    <meta name="keywords" lang="en" content="">
    <meta name="description" lang="en" content="">


    <!--  CSS  -->
    <link rel="stylesheet" href="<?=asset('css/sb-admin-2/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=asset('font-awesome-4.2.0/css/font-awesome.min.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/custom.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/sb-admin-2/css/plugins/metisMenu/metisMenu.min.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/sb-admin-2/css/plugins/timeline.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/sb-admin-2/css/sb-admin-2.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/sb-admin-2/css/plugins/morris.css')?>"/>



    <!-- SCRIPTS -->
    <script src="<?=asset('js/jquery-1.9.min.js')?>"></script>
    <script src="<?=asset('js/custom.js')?>"></script>
    <script src="<?=asset('bootstrap-3.1.0/bootstrap-3.1.0/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?=asset('bootstrap-3.1.0/bootstrap-3.1.0/js/tooltip.js')?>"></script>
    <script type="text/javascript" src="<?=asset('bootstrap-3.1.0/bootstrap-3.1.0/docs/assets/js/vendor/holder.js')?>"></script>
    <script src="<?=asset('css/sb-admin-2/js/sb-admin-2.js')?>"></script>
    <script src="<?=asset('css/sb-admin-2/js/sb-admin-2.js')?>"></script>
    <script src="<?=asset('css/sb-admin-2/js/plugins/metisMenu/metisMenu.min.js')?>"></script>
    <script src="<?=asset('css/sb-admin-2/js/plugins/morris/raphael.min.js')?>"></script>
<!--    <script src="--><?//=asset('css/sb-admin-2/js/plugins/morris/morris.min.js')?><!--"></script>-->
<!--    <script src="--><?//=asset('css/sb-admin-2/js/plugins/morris/morris-data.js')?><!--"></script>-->
    <script src="<?=asset('css/sb-admin-2/js/sb-admin-2.js')?>"></script>






    @yield('head')

</head>

<title>Administrare</title>

<body>

@yield('content')

<footer>
    @yield('footer')
</footer>



</body>


</html>