@extends('layouts.master_cms')

@section('head')
<!--Here goes general css and scripts-->
@yield('head_1')
@endsection

@section('content')

@yield('content_1')

@endsection


@section('footer')
@yield('footer_1')
<div class="container-fluid vertical_align">
    <div class="container ">
        <div class="footer">
            &copy 2014 DentalArt
        </div>
    </div>
</div>


@endsection