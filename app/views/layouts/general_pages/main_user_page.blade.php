@extends('layouts.master_page')

@section('head')
    <!--Here goes general css and scripts-->
    @yield('head_1')
@endsection

@section('content')
    
    <div class="container-fluid">
    	<div class="row">
    		<div class="grey_bar height_10"></div>
    	</div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">
                                <img class="padding_top_20" src="<?=asset('images/logo.png')?>">
                            </div>
                            <div class="col-sm-9 padding_left_60">
                                <div class="row">
                                    <h3>Doctor Stomatolog Szabo Cristian</h3></br>
                                </div>

                                <div class="row">
                                    <ul class="nav nav-pills">
                                        <li class="{{ Request::is('/') ? 'active' : '' }} {{ Request::is('homepage') ? 'active' : '' }}"><a href="homepage">Acasa</a></li>
                                        <li class="{{ Request::is('servicii_profesionale') ? 'active' : '' }}"><a href="servicii_profesionale" >Servicii Profesionale</a></li>
                                        <li class="{{Request::is('parteneri') ? 'active' : '' }}"><a href="parteneri" >Parteneri</a></li>
                                        <li class="{{Request::is('contact') ? 'active' : '' }}"><a href="contact" >Contact</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="row">

                        </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12 padding_top_10 padding_bottom_10">
                         Cabinet stomatologic
                     </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 ">
                <div class="row margin_top_10">
                    <a href="#" class="margin_bottom_-20 thumbnail width_120  pull-right">
                        <img src="<?=asset('images/doctor.jpg')?>" alt="My picture Here">
                    </a>
                </div>
                <div class="row">
                    <p class="margin_top_20 pull-right"> <i>Sanatatea zambeste frumos.</i> </p>
                </div>
                
                
            </div>
        </div>
    </div>

	<div class="container-fluid">
		<div class="row blue_bar"></div>
	</div>
	@yield('content_1')
@endsection

@section('footer')
    @yield('footer_1')
    <div class="container-fluid vertical_align">
        <div class="container ">
            <div class="footer">
                &copy 2014 DentalArt
            </div>
        </div>
    </div>

    
@endsection