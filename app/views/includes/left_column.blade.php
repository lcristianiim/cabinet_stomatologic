<div class="col-sm-3">

    @foreach ($doctors as $doctor)
    @if ($doctor->visibility == 1)
    <div class="row">
        <hr>
        <div class="panel panel-default ">
            <!-- Default panel contents -->
            <div class="panel-heading">Program de lucru <span class="text-muted title_small">dr. {{$doctor->last_name . " " . $doctor->first_name}}</span></div>

            <!-- List group -->
            <ul class="list-group">

                <li class="list-group-item">
                    <table class="table table-bordered">
                        <tbody>

                        <tr><h4 class="text-center">Doctor {{$doctor->last_name . " " . $doctor->first_name}}</h4></tr>


                        @foreach($days_of_the_week as $day)
                        <?php
                            $var1 = $day.'_schedule_start';
                            $var2 = $day.'_schedule_end';
                        ?>
                        @if (!empty($doctor->$var1) & !empty($doctor->$var2))
                        <tr>
                            <td><p class="text-center margin_bottom_0">{{$day}}:</p></td>
                            <td><p class="text-center margin_bottom_0">{{$doctor->$var1 . ' - ' . $doctor->$var2 }}</p></td>
                        </tr>
                        @endif

                        @endforeach


                        </tbody>

                    </table>
                </li>

            </ul>
        </div>
    </div>
    @endif
    @endforeach




</div>