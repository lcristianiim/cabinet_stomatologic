<div class="col-sm-3">

    <div class="row">
        <hr/>
        <div class="panel panel-default margin_top_15">
            <!-- Default panel contents -->
            <div class="panel-heading">Programari telefonice</div>

            <!-- List group -->
            <ul class="list-group">

                <li class="list-group-item">
                    <table class="table-condensed">
                        <tbody>
                        <tr><h4 class="text-center">Zilnic intre {{$phone_schedule->start_time}} - {{$phone_schedule->end_time}}</h4></tr>
                        </tbody>
                    </table>
                    <p class="text-center">{{$phone_schedule->phone_1}}</p>
                    <p class="text-center">{{$phone_schedule->phone_2}}</p>
                </li>
            </ul>
        </div>
    </div>
    <hr>
    <div class="panel panel-default margin_top_15">
        <!-- List group -->
        <ul class="list-group">

            <li class="list-group-item">
                <h4><span>De ce cabinetul nostru?</span></h4>
            </li>

            @foreach ($why_us as $item)
            <li class="list-group-item">
                <p>{{$item->content}}</p>
            </li>
            @endforeach

        </ul>
    </div>
    <hr>

</div>