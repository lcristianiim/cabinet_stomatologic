<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Administrare Web Cabinet Stomatologic Szabo Cristian</a>
    </div>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="doctori" class="{{ Request::is('doctori') ? 'active' : '' }}">Doctori</a>

                </li>
                <li>
                    <a href="programari_telefonice" class="{{ Request::is('programari_telefonice') ? 'active' : '' }}">Programari telefonice</a>

                </li>

                <li>
                    <a href="descriere_cabinet" class="{{ Request::is('descriere_cabinet') ? 'active' : '' }}">Descriere cabinet</a>

                </li>
                <li>
                    <a href="servicii_profesionale_cms" class="{{ Request::is('servicii_profesionale_cms') ? 'active' : '' }}">Servicii profesionale</a>

                </li>
                <li>
                    <a href="partners" class="{{ Request::is('partners') ? 'active' : '' }}">Parteneri</a>

                </li>
                <li>
                    <a href="de_ce_cabinetul_nostru" class="{{ Request::is('de_ce_cabinetul_nostru') ? 'active' : '' }}">De ce cabinetul nostru</a>

                </li>
                <li>
                    <a href="contact_cms" class="{{ Request::is('contact_cms') ? 'active' : '' }}">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>