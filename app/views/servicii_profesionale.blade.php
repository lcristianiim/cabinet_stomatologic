@extends('layouts.general_pages.main_user_page')

@section('head_1')
<!--Here goes specific css and scripts-->

@endsection

@section('content_1')
<div class="container margin_top_15">

    <div class="row">

        <!--include starts with a col-sm-3 div-->
        @include('includes.left_column')

        <div class="col-sm-6">

            <div class="row-fluid">
                <h4>Servicii profesionale</h4>
                @foreach($descriptions as $description)
                <h3><small>{{$description->title}}</small></h3>
                <p>{{$description->text}}</p>
                @endforeach

            </div>

            <hr>

            <div class="row-fluid">

                    @if (isset($services[0]))
                    <table class="table table-striped">
                        <thead>
                            <tr><th>Tratamente si servicii</th></tr>
                        </thead>

                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td>{{$service->name}}</td>
                                <td>{{$service->price}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
            </div>
            <hr/>
        </div>

        <!--include starts with a col-sm-3 div-->
        @include('includes.right_column')

    </div>

</div>
@endsection

@section('footer_1')
<!--specific elements of the footer-->

@endsection