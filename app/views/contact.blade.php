@extends('layouts.general_pages.main_user_page')

@section('head_1')
<!--Here goes specific css and scripts-->

@endsection

@section('content_1')
<div class="container margin_top_15">

    <div class="row">

        <!--include starts with a col-sm-3 div-->
        @include('includes.left_column')

        <div class="col-sm-6">
            <div class="row-fluid">
                <h4>Contact</h4>
                <address>
                    <strong>{{$contact->name}}</strong><br>
                    strada {{$contact->street}} {{$contact->number}}, Cartierul {{$contact->neighborhood}}<br>
                    {{$contact->city}}, Cod Postal: {{$contact->postal_code}}<br><br/>
                    <abbr title="Telefon">Telefon:</abbr> {{$contact->phone}} <br/>
                    <abbr title="Telefon">Fax:</abbr> {{$contact->fax}}
                </address>
            </div>


            <div class="row-fluid">
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:400px;width:500px;"><div id="gmap_canvas" style="height:400px;width:500px;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.com/sheego-gutschein/" id="get-map-data">map-embed.com gutschein</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:17,center:new google.maps.LatLng(47.06603339999999,21.92796599999997),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(47.06603339999999, 21.92796599999997)});infowindow = new google.maps.InfoWindow({content:"<b>Cabinet Stomatologic</b><br/>str. Menumorut 46<br/> oradea" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
            </div>


        </div>

        <!--include starts with a col-sm-3 div-->
        @include('includes.right_column')

    </div>

</div>
@endsection

@section('footer_1')
<!--specific elements of the footer-->

@endsection