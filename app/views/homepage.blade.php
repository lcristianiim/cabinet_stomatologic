@extends('layouts.general_pages.main_user_page')

@section('head_1')
    <!--Here goes specific css and scripts-->

@endsection

@section('content_1')
	<div class="container margin_top_15">
		
		<div class="row">

            <!--include starts with a col-sm-3 div-->
            @include('includes.left_column')

			<div class="col-sm-6">
				<h4>{{$contact->name}}</h4>
				@foreach ($descriptions as $description)
                <h3><small>{{$description->title}}</small></h3>
                <p>{{$description->text}}</p>
                @endforeach
			</div>

            <!--include starts with a col-sm-3 div-->
            @include('includes.right_column')

		</div>
		
	</div>
@endsection

@section('footer_1')
<!--specific elements of the footer-->

@endsection