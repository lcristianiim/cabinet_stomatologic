@extends('layouts.general_pages.main_user_page')

@section('head_1')
<!--Here goes specific css and scripts-->

@endsection

@section('content_1')
<div class="container margin_top_15">

    <div class="row">

        <!--include starts with a col-sm-3 div-->
        @include('includes.left_column')

        <div class="col-sm-6">

            <div class="row-fluid">
                <h4>Parteneri</h4>
                @foreach($descriptions as $description)
                <h3><small>{{$description->title}}</small></h3>
                <p>{{$description->text}}</p>
                @endforeach
            </div>

            <hr>

            <div class="row-fluid">
                <table class="table table-striped">

                    <thead>
                    <tr>
                        <th>Parteneri</th>
                        <th>Link</th>
                    </tr>
                    </thead>

                    <tbody>

                   @foreach($partners as $partner)
                   <tr>
                       <td>
                           {{$partner->name}}
                       </td>
                       <td>
                           <a href="{{$partner->link}}">www.rodenta.com</a>
                       </td>
                   </tr>
                   @endforeach

                    @for($i=0;$i<=10;$i++)

                    @endfor


                    </tbody>

                </table>

            </div>
            <hr/>
        </div>

        <!--include starts with a col-sm-3 div-->
        @include('includes.right_column')

    </div>

</div>
@endsection

@section('footer_1')
<!--specific elements of the footer-->

@endsection