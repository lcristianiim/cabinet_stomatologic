@extends('layouts.master_cms')

@section('content')

<div id="wrapper">

    @include('includes.navigation_cms')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Servicii profesionale - sectiunea "Servicii Profesionale"</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Formular de adaugare o noua descriere despre servicii</div>

                    <!-- List group -->
                    <ul class="list-group">

                        <li class="list-group-item">
                            <table class="table-condensed">
                                <tbody>
                                {{ Form::open(array(
                                'url' => 'adauga_descriere_servicii_noua',
                                'role' => 'form'
                                )) }}

                                <div class="form-group text-left">
                                    <label for="last_name">Titlu:</label>
                                    {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Titlu', 'required' => 'true')) }}
                                </div>

                                <div class="form-group text-left">
                                    <label for="last_name">Continut:</label>
                                    {{ Form::textarea('content', null, array('class' => 'form-control', 'placeholder' => 'Continut', 'required' => 'true')) }}
                                </div>

                                <div class="form-group text-left">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Inregistreaza descrierea</button>
                                    </div>
                                </div>

                                {{ Form::close(); }}

                                </tbody>
                            </table>

                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-6">

                <div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">
                    @foreach ($descriptions as $description)
                    <div class="panel panel-default">

                        <div id="heading_{{$description->id}}" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseOne_{{$description->id}}" aria-expanded="false" href="#collapseOne_{{$description->id}}" data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                    {{$description->title}}
                                </a>
                            </h4>
                        </div>

                        <div aria-labelledby="heading_{{$description->id}}" role="tabpanel" class="panel-collapse collapse" id="collapseOne_{{$description->id}}" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                {{$description->text}}
                            </div>
                            <div class="panel-body">
                                <!--Delete Description-->
                                <a data-toggle="modal" data-target="#myModalTrash{{$description->id}}" href="#">
                                    <i class="fa fa-trash text_red"></i>
                                </a>

                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalTrash{{$description->id}}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                <h4 id="myModalLabel" class="modal-title">Stergerea descrierei "{{$description->title}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><strong>{{$description->title}}</strong></p>
                                                <p>{{$description->text}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button data-dismiss="modal" class="btn btn-default" type="button">Inchide</button>
                                                <a href="sterge_descrierea_serviciului/{{$description->id}}"><button class="btn btn-primary" type="button">Stergere</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!--Edit Description-->
                                <a data-toggle="modal" data-target="#myModalEdit{{$description->id}}" href="#">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEdit{{$description->id}}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                <h4 id="myModalLabel" class="modal-title">Editarea descrierei "{{$description->title}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{ Form::open(array(
                                                'url' => 'editare_descriere_serviciu/' . $description->id,
                                                'role' => 'form'
                                                )) }}

                                                <div class="form-group text-left">
                                                    <label for="last_name">Titlu:</label>
                                                    {{ Form::text('title', $description->title, array('class' => 'form-control', 'placeholder' => 'Titlu', 'required' => 'true')) }}
                                                </div>

                                                <div class="form-group text-left">
                                                    <label for="last_name">Continut:</label>
                                                    {{ Form::textarea('content', $description->text, array('class' => 'form-control', 'placeholder' => 'Continut', 'required' => 'true')) }}
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button data-dismiss="modal" class="btn btn-default" type="button">Inchide</button>
                                                <button class="btn btn-primary" type="submit">Modifica datele</button>
                                                {{Form::close();}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                    @endforeach

                </div>


            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Formular de adaugare servicii</div>

                    <!-- List group -->
                    <ul class="list-group">

                        <li class="list-group-item">
                            <table class="table-condensed">
                                <tbody>
                                {{ Form::open(array(
                                'url' => 'adauga_serviciu_nou',
                                'role' => 'form'
                                )) }}

                                <div class="form-group text-left">
                                    <label for="last_name">Nume serviciu:</label>
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Numele serviciului', 'required' => 'true')) }}
                                </div>

                                <div class="form-group text-left">
                                    <label for="last_name">Pret:</label>
                                    {{ Form::text('price', null, array('class' => 'form-control', 'placeholder' => 'Pretul serviciului in lei', 'required' => 'true')) }}
                                </div>

                                <div class="form-group text-left">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Inregistreaza serviciu</button>
                                    </div>
                                </div>

                                {{ Form::close(); }}

                                </tbody>
                            </table>

                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-6">

                <div aria-multiselectable="true" role="tablist" id="accordion1" class="panel-group">
                    @foreach ($services as $service)
                    <div class="panel panel-default">

                        <div id="heading1_{{$service->id}}" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseOne_{{$service->id}}" aria-expanded="false" href="#collapseOne1_{{$service->id}}" data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                    {{$service->name}} | {{$service->price}}
                                </a>
                            </h4>
                        </div>

                        <div aria-labelledby="heading1_{{$service->id}}" role="tabpanel" class="panel-collapse collapse" id="collapseOne1_{{$service->id}}" aria-expanded="false" style="height: 0px;">

                            <div class="panel-body">
                                <!--Delete Description-->
                                <a data-toggle="modal" data-target="#myModalTrash1{{$service->id}}" href="#">
                                    <i class="fa fa-trash text_red"></i>
                                </a>

                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalTrash1{{$service->id}}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                <h4 id="myModalLabel" class="modal-title">Stergerea descrierei "{{$service->name}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><strong>{{$service->name}}</strong></p>
                                                <p>{{$service->price}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button data-dismiss="modal" class="btn btn-default" type="button">Inchide</button>
                                                <a href="sterge_serviciul/{{$service->id}}"><button class="btn btn-primary" type="button">Stergere</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!--Edit Description-->
                                <a data-toggle="modal" data-target="#myModalEdit1{{$service->id}}" href="#">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEdit1{{$service->id}}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                <h4 id="myModalLabel1" class="modal-title">Editarea descrierei "{{$service->name}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{ Form::open(array(
                                                'url' => 'editare_serviciu/' . $service->id,
                                                'role' => 'form'
                                                )) }}

                                                <div class="form-group text-left">
                                                    <label for="last_name">Numele serviciului:</label>
                                                    {{ Form::text('name', $service->name, array('class' => 'form-control', 'placeholder' => 'Numele serviciului', 'required' => 'true')) }}
                                                </div>

                                                <div class="form-group text-left">
                                                    <label for="last_name">Pretul:</label>
                                                    {{ Form::text('price', $service->price, array('class' => 'form-control', 'placeholder' => 'Pretul serviciului', 'required' => 'true')) }}
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button data-dismiss="modal" class="btn btn-default" type="button">Inchide</button>
                                                <button class="btn btn-primary" type="submit">Modifica datele</button>
                                                {{Form::close();}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                    @endforeach

                </div>

        </div>

    </div> <!-- /.row -->
    <!-- /#page-wrapper -->

</div>

@endsection