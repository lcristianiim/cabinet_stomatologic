@extends('layouts.master_cms')

@section('content')

<div id="wrapper">

@include('includes.navigation_cms')

<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Programari telefonice</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">

<div class="col-sm-4">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Situatia actuala programarilor telefonice</div>

        <!-- List group -->
        <ul class="list-group">

            <li class="list-group-item">
                <table class="table-condensed">
                    <tbody>

                    @if ( $phone_schedule_set == 0 )
                    <tr>
                        <div role="alert" class="alert alert-danger">
                            <h5 class="text-center"><strong>Atentie!</strong> Programul pentru programari telefonice nu este setat.</h5>
                        </div>
                    </tr>
                    @else
                    <tr>
                        <h4 class="text-center">Zilnic intre: {{$start_time}} : {{$end_time}}</h4>
                        <p class="text-center">{{$phone_1}}</p>
                        <p class="text-center">{{$phone_2}}</p>
                    </tr>

                    @endif

                    </tbody>
                </table>

            </li>
        </ul>
    </div>
</div>

<div class="col-sm-4">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Modifica programul pentru programarile telefonice</div>

        <!-- List group -->
        <ul class="list-group">

            <li class="list-group-item">
                <table class="table-condensed">
                    <tbody>
                    {{ Form::open(array(
                    'url' => 'edit_phone_schedule',
                    'role' => 'form'
                    )) }}

                    <div class="form-group text-left">
                        <label for="last_name">Inceputul programului pentru programari:</label>
                        {{ Form::text('start_time', $start_time, array('class' => 'form-control', 'placeholder' => 'Inceputul programului pentru programari', 'required' => 'true')) }}
                    </div>

                    <div class="form-group text-left">
                        <label for="last_name">Sfarsitul programului pentru programari:</label>
                        {{ Form::text('end_time', $end_time, array('class' => 'form-control', 'placeholder' => 'Sfarsitul programului pentru programari', 'required' => 'true')) }}
                    </div>

                    <div class="form-group text-left">
                        <label for="last_name">Telefon fix:</label>
                        {{ Form::text('phone_1', $phone_1, array('class' => 'form-control', 'placeholder' => 'Telefon fix', 'required' => 'true')) }}
                    </div>

                    <div class="form-group text-left">
                        <label for="last_name">Telefon mobil:</label>
                        {{ Form::text('phone_2', $phone_2, array('class' => 'form-control', 'placeholder' => 'Telefon mobil', 'required' => 'true')) }}
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Modifica</button>
                    </div>



                    {{ Form::close()}}
                    </tbody>
                </table>

            </li>
        </ul>
    </div>

</div>




<!-- /.row -->

</div>
<!-- /#page-wrapper -->

</div>

@endsection