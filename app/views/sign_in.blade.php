<html lang="en"><head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="../../favicon.ico" rel="icon">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?=asset('bootstrap-3.1.0/bootstrap-3.1.0/dist/css/bootstrap.min.css')?>"/>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?=asset('css/signin.css')?>">


    <![endif]-->
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            {{ Form::open(array('url' => 'authenticate', 'class' => 'form-signin', 'role' => 'form')) }}

            <h2 class="form-signin-heading">Autentificare</h2>
            <h3><small>Cabinet Stomatologic Oradea</small></h3>
            <input name="email" type="email" autofocus="" required="" placeholder="Email address" class="form-control">
            <input name="password" type="password" required="" placeholder="Password" class="form-control">
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
            </label>
            <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>

            {{ Form::close() }}
        </div>

    </div>

    @if(isset($input))
        <div class="row margin_top_30">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="alert alert-danger center-block text-center" role="alert">Autentificare nereusita. Verificati userul si parola.</div>
            </div>

        </div>
    @endif


</div> <!-- /container -->




</body></html>