@extends('layouts.master_cms')

@section('content')

<div id="wrapper">

    @include('includes.navigation_cms')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Doctori</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-sm-12">


            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-book fa-fw"></i> Doctori inregistrati</div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th  class="text-center">Nume</th>
                            <th class="text-center">Editare</th>


                            @foreach($days_of_the_week as $day)
                            <th class="text-center">{{$day}}</th>
                            @endforeach
                            <th class="text-center">Duminica</th>

                        </tr>
                        </thead>
                        <tbody class="text-center">




                        @foreach ($doctors as $doctor)
                        <tr>
                            <td>{{$doctor->last_name . " " . $doctor->first_name}}</td>

                            <td>
                                <div class="row-fluid">
                                    <div class="col-sm-6">
                                        <a href="#" data-target="#myModalTrash<?= $doctor->id ?>" data-toggle="modal" >
                                            <i class="fa fa-trash text_red"></i>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" data-target="#myModalEdit<?= $doctor->id ?>" data-toggle="modal" >
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </div>


                                <!--Modals-->
                                <div class="modal fade" id="myModalTrash<?= $doctor->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Stergerea doctorului <?= $doctor->first_name . " " . $doctor->last_name ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Continuati pe mai departe pentru stergerea doctorului <strong><?= $doctor->first_name . " " . $doctor->last_name ?></strong></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                                <a href="delete_doctor/<?=$doctor->id?>"><button type="button" class="btn btn-primary">Stergere</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="myModalEdit<?= $doctor->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Modificarea datelor doctorului <?= $doctor->first_name . " " . $doctor->last_name ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row-fluid">

                                                {{ Form::open(array(
                                                    'url' => 'edit_doctor/'. $doctor->id,
                                                    'role' => 'form'
                                                )) }}
                                                    <div class="form-group text-left">
                                                        <label for="last_name">Nume:</label>
                                                        {{ Form::text('last_name', $doctor->first_name, array('class' => 'form-control', 'placeholder' => 'Numele..', 'required' => 'true')) }}
                                                    </div>

                                                    <div class="form-group text-left">
                                                        <label for="first_name">Prenume:</label>
                                                        {{ Form::text('first_name', $doctor->last_name, array('class' => 'form-control', 'placeholder' => 'Prenumele..', 'required' => 'true')) }}
                                                    </div>

                                                    <div class="form-group text-left">
                                                        <label for="first_name">Telefon:</label>
                                                        {{ Form::text('phone', $doctor->phone, array('class' => 'form-control', 'placeholder' => 'Telefon..', 'required' => 'true')) }}
                                                    </div>

                                                    <div class="form-group text-left">
                                                        <label for="first_name">Email:</label>
                                                        {{ Form::email('email', $doctor->email, array('class' => 'form-control', 'placeholder' => 'Email..', 'required' => 'true')) }}
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="panel panel-default margin_top_15">
                                                            <!-- List group -->
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <!-- /.panel-heading -->
                                                                    <div class="panel-body">
                                                                        <!-- Nav tabs -->
                                                                        <ul class="nav nav-pills">
                                                                            <li class=""><a data-toggle="tab" href="#Luni-pills_<?= $doctor->id ?>">Luni</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Marti-pills_<?= $doctor->id ?>">Marti</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Miercuri-pills_<?= $doctor->id ?>">Miercuri</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Joi-pills_<?= $doctor->id ?>">Joi</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Vineri-pills_<?= $doctor->id ?>">Vineri</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Sambata-pills_<?= $doctor->id ?>">Sambata</a>
                                                                            </li>
                                                                            <li class=""><a data-toggle="tab" href="#Duminica-pills_<?= $doctor->id ?>">Duminica</a>
                                                                            </li>
                                                                        </ul>

                                                                        <!-- Tab panes -->
                                                                        <div class="tab-content">
                                                                            <div id="home-pills_<?= $doctor->id ?>" class="tab-pane fade active in">
                                                                                <h4 class="text-center">Selecteaza ziua</h4>
                                                                                <p class="text-center">Selecteaza ziua si stabileste programul de lucru.</p>
                                                                            </div>

                                                                            @foreach($days_of_the_week as $day)
                                                                            <div id="{{$day}}-pills_<?= $doctor->id ?>" class="tab-pane fade">
                                                                                <hr/>
                                                                                <h4 class="text-center">{{$day}}</h4>
                                                                                <p class="text-center">{{Form::checkbox($day . '-free', 'value', true, array('class'=>$day . '-free'));}} liber {{$day}}</p>
                                                                                <hr/>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label>De la:</label>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <label for="">Ora</label>
                                                                                                    <select name="{{$day}}_schedule_start_hour<?= $doctor->id ?>" class="form-control {{$day}}_schedule_start_hour">
                                                                                                        <option>-</option>
                                                                                                        @for($i=8;$i<=20;$i++)
                                                                                                        <option>{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <label for="">Min</label>
                                                                                                    <select name="{{$day}}_schedule_start_min<?= $doctor->id ?>" class="form-control {{$day}}_schedule_start_min">
                                                                                                        <option>00</option>
                                                                                                        <option>15</option>
                                                                                                        <option>30</option>
                                                                                                        <option>45</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label>Pana la:</label>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <label for="">Ora</label>
                                                                                                    <select name="{{$day}}_schedule_end_hour<?= $doctor->id ?>" class="form-control {{$day}}_schedule_end_hour">
                                                                                                        <option>-</option>
                                                                                                        @for($i=8;$i<=20;$i++)
                                                                                                        <option>{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <label for="">Min</label>
                                                                                                    <select name="{{$day}}_schedule_end_min<?= $doctor->id ?>" class="form-control {{$day}}_schedule_end_min">
                                                                                                        <option>00</option>
                                                                                                        <option>15</option>
                                                                                                        <option>30</option>
                                                                                                        <option>45</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>



                                                                            </div>
                                                                            @endforeach


                                                                            <div id="Duminica-pills1" class="tab-pane fade">
                                                                                <h4 class="text-center">Duminica</h4>
                                                                                <p class="text-center">Duminica liber.</p>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <!-- /.panel-body -->

                                                                </li>
                                                            </ul>
                                                        </div>

                                                    </div>


                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                                <button type="submit" class="btn btn-primary">Modifica</button>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </td>


                            @foreach ($days_of_the_week as $day)
                            <?php

                                $var1 = $day.'_schedule_start';
                                $var2 = $day.'_schedule_end';

                                if (empty($doctor->$var1) & empty($doctor->$var2)){
                                    echo "<td>" . "liber" . "</td>";
                                } else {

                                    echo "<td>" .
                                        $doctor->$var1 . ' - ' . $doctor->$var2 .
                                        "</td>";
                                }

                            ?>


                            @endforeach


                            <td>liber</td>

                        </tr>

                        @endforeach





                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>

        <div class="row">
            <div class="col-sm-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-plus"></i> Formular de adaugare doctori
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row-fluid">
                            {{ Form::open(array(
                                'url' => 'new_doctor_store',
                                'role' => 'form'
                            ))}}

                            <div class="form-group">
                                <label for="first_name">Nume:</label>
                                {{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => 'Numele..', 'required' => 'true')) }}
                                {{$errors->first('first_name', '<span class=error>:message</span>' ) }}


                            </div>

                            <div class="form-group">
                                <label for="last_name">Prenume:</label>
                                {{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => 'Prenumele..', 'required' => 'true')) }}
                                {{$errors->first('last_name', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                <label for="phone">Telefon:</label>
                                {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Telefon..', 'required' => 'true')) }}
                                {{$errors->first('phone', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                <label for="phone">Email:</label>
                                {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email..', 'required' => 'true')) }}
                                {{$errors->first('email', '<span class=error>:message</span>' ) }}
                            </div>


                            <div class="form-group">
                                <div class="panel panel-default margin_top_15">
                                    <!-- List group -->
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-pills">
                                                    <li class=""><a data-toggle="tab" href="#Luni-pills">Luni</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Marti-pills">Marti</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Miercuri-pills">Miercuri</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Joi-pills">Joi</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Vineri-pills">Vineri</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Sambata-pills">Sambata</a>
                                                    </li>
                                                    <li class=""><a data-toggle="tab" href="#Duminica-pills">Duminica</a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div id="home-pills" class="tab-pane fade active in">
                                                        <h4 class="text-center">Selecteaza ziua</h4>
                                                        <p class="text-center">Selecteaza ziua si stabileste programul de lucru.</p>
                                                    </div>

                                                    @foreach($days_of_the_week as $day)
                                                    <div id="{{$day}}-pills" class="tab-pane fade">
                                                        <hr/>
                                                        <h4 class="text-center">{{$day}}</h4>
                                                        <p class="text-center">{{Form::checkbox($day . '-free', 'value', true, array('class'=>$day . '-free'));}} liber {{$day}}</p>
                                                        <hr/>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label>De la:</label>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <label for="">Ora</label>
                                                                            <select name="{{$day}}_schedule_start_hour" class="form-control {{$day}}_schedule_start_hour">
                                                                                <option>-</option>
                                                                                @for($i=8;$i<=20;$i++)
                                                                                <option>{{$i}}</option>
                                                                                @endfor
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label for="">Min</label>
                                                                            <select name="{{$day}}_schedule_start_min" class="form-control {{$day}}_schedule_start_min">
                                                                                <option>00</option>
                                                                                <option>15</option>
                                                                                <option>30</option>
                                                                                <option>45</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Pana la:</label>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <label for="">Ora</label>
                                                                            <select name="{{$day}}_schedule_end_hour" class="form-control {{$day}}_schedule_end_hour">
                                                                                <option>-</option>
                                                                                @for($i=8;$i<=20;$i++)
                                                                                <option>{{$i}}</option>
                                                                                @endfor
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label for="">Min</label>
                                                                            <select name="{{$day}}_schedule_end_min" class="form-control {{$day}}_schedule_end_min">
                                                                                <option>00</option>
                                                                                <option>15</option>
                                                                                <option>30</option>
                                                                                <option>45</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr/>



                                                    </div>
                                                    @endforeach


                                                    <div id="Duminica-pills" class="tab-pane fade">
                                                        <h4 class="text-center">Duminica</h4>
                                                        <p class="text-center">Duminica liber.</p>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- /.panel-body -->

                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-default center-block">Adauga doctor</button>

                            {{ Form::close() }}
                        </div>

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>

        </div>



        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->

</div>

@endsection