@extends('layouts.master_cms')

@section('content')

<div id="wrapper">

    @include('includes.navigation_cms')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Datele de Contact - Sectiunea "Contact"</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Date de contact ale cabinetului stomatologic</div>

                    <!-- List group -->
                    <ul class="list-group">

                        <li class="list-group-item">

                            <h4 class="text-center">Numele cabinetului stomatologic:</h4>
                            <p class="text-center">{{$contact->name}}</p>

                            <hr/>

                            <h4 class="text-center">Strada:</h4>
                            <p class="text-center">{{$contact->street}}</p>

                            <hr/>

                            <h4 class="text-center">Cartier:</h4>
                            <p class="text-center">{{$contact->neighborhood}}</p>

                            <hr/>

                            <h4 class="text-center">Oras:</h4>
                            <p class="text-center">{{$contact->city}}</p>

                            <hr/>

                            <h4 class="text-center">Cod Postal:</h4>
                            <p class="text-center">{{$contact->postal_code}}</p>

                            <hr/>

                            <h4 class="text-center">Telefon:</h4>
                            <p class="text-center">{{$contact->phone}}</p>

                            <hr/>

                            <h4 class="text-center">Fax:</h4>
                            <p class="text-center">{{$contact->fax}}</p>


                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Modifica programul pentru programarile telefonice</div>

                    <!-- List group -->
                    <ul class="list-group">

                        <li class="list-group-item">
                            {{ Form::open(array(
                            'url' => 'edit_contact_details',
                            'role' => 'form'
                            )) }}

                            <div class="form-group text-left">
                                <label for="last_name">Numele cabinetului:</label>
                                <input type="text" value="{{$contact->name}}" name="name" required="true" placeholder="Numele cabinetului" class="form-control">
                            </div><div class="form-group text-left">
                                <label for="last_name">Strada:</label>
                                <input type="text" value="{{$contact->street}}" name="street" required="true" placeholder="Strada" class="form-control">
                            </div><div class="form-group text-left">
                                <label for="last_name">Cartier:</label>
                                <input type="text" value="{{$contact->neighborhood}}" name="neighborhood" required="true" placeholder="Cartier" class="form-control">
                            </div><div class="form-group text-left">
                                <label for="last_name">Oras:</label>
                                <input type="text" value="{{$contact->city}}" name="city" required="true" placeholder="Orasul" class="form-control">
                            </div>
                            <div class="form-group text-left">
                                <label for="last_name">Cod Postal:</label>
                                <input type="text" value="{{$contact->postal_code}}" name="postal_code" required="true" placeholder="Codul Postal" class="form-control">
                            </div>

                            <div class="form-group text-left">
                                <label for="last_name">Telefon:</label>
                                <input type="text" value="{{$contact->phone}}" name="phone" required="true" placeholder="Telefon" class="form-control">
                            </div>

                            <div class="form-group text-left">
                                <label for="last_name">Fax:</label>
                                <input type="text" value="{{$contact->fax}}" name="fax" required="true" placeholder="Fax" class="form-control">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Modifica</button>
                            </div>

                            {{Form::close()}}

                        </li>
                    </ul>
                </div>

            </div>




            <!-- /.row -->

        </div>

    </div> <!-- /.row -->
    <!-- /#page-wrapper -->

</div>

@endsection